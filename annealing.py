import math
import random

import multiprocessing


class Annealable:
    def objective_function(self, solution) -> float:
        raise NotImplementedError

    def get_starting_solution(self):
        raise NotImplementedError

    def get_modified_solution(self, base_solution, allowed_variance):
        raise NotImplementedError

    def get_max_iterations_without_improvement(self) -> int:
        raise NotImplementedError


class Annealer:
    BETA = 0.01  # temperature reducing factor
    INITIAL_TEMPERATURE = 1  # initial temperature

    def anneal(self, annealable: Annealable):
        iterations_without_improvement = 0
        current_solution = annealable.get_starting_solution()
        temperature = self.INITIAL_TEMPERATURE
        max_iterations = annealable.get_max_iterations_without_improvement()
        while iterations_without_improvement < max_iterations:
            current_solution_objective_function_value = annealable.objective_function(current_solution)
            modified_solution = annealable.get_modified_solution(current_solution,
                                                                 current_solution_objective_function_value)
            modified_solution_objective_function_value = annealable.objective_function(modified_solution)
            delta_objective = modified_solution_objective_function_value - current_solution_objective_function_value
            if delta_objective < 0 or random.random() < math.e ** ((-delta_objective - 0.1) / temperature):
                current_solution = modified_solution
                iterations_without_improvement = 0
            else:
                iterations_without_improvement += 1
            temperature = self.reduce_temperature(temperature)

        return current_solution

    def reduce_temperature(self, temperature):
        return temperature / (1 + self.BETA * temperature)


class MultiAnnealer(Annealer):
    def anneal(self, annealable: Annealable):
        with multiprocessing.Pool() as pool:
            iterations_without_improvement = 0
            current_solution = annealable.get_starting_solution()
            temperature = self.INITIAL_TEMPERATURE
            max_iterations = int(math.sqrt(annealable.get_max_iterations_without_improvement()))
            while iterations_without_improvement < max_iterations:
                current_solution_objective_function_value = annealable.objective_function(current_solution)
                args = [(max_iterations, annealable, current_solution, temperature, self) for i in range(8)]
                results = pool.map(find_solution, args)
                results_with_obj_funcs = [(r, annealable.objective_function(r[0]))
                                          for r in results]
                modified_solution, modified_solution_objective_function_value = sorted(results_with_obj_funcs, key=lambda x: x[1])[0]
                modified_solution, temperature = modified_solution
                delta_objective = modified_solution_objective_function_value - current_solution_objective_function_value
                if delta_objective < 0 or random.random() < math.e ** ((-delta_objective - 0.1) / temperature):
                    current_solution = modified_solution
                    iterations_without_improvement = 0
                else:
                    iterations_without_improvement += 1
                # temperature = self.reduce_temperature(temperature)

        return current_solution

    def reduce_temperature(self, temperature):
        return temperature / (1 + self.BETA * temperature)


def find_solution(args):
    max_iterations, annealable, current_solution, starting_temperature, annealer = args
    iterations_without_improvement = 0
    temperature = starting_temperature
    while iterations_without_improvement < max_iterations:
        current_solution_objective_function_value = annealable.objective_function(
            current_solution)
        modified_solution = annealable.get_modified_solution(current_solution,
                                                             current_solution_objective_function_value)
        modified_solution_objective_function_value = annealable.objective_function(
            modified_solution)
        delta_objective = modified_solution_objective_function_value - current_solution_objective_function_value
        if delta_objective < 0 or random.random() < math.e ** ((-delta_objective - 0.1) / temperature):
            current_solution = modified_solution
            break
        iterations_without_improvement += 1
        temperature = annealer.reduce_temperature(temperature)
    return current_solution, temperature
