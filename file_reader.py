import json


def read_data_from_file(path):
    with open(path, "r") as fp:
        content = json.load(fp)
    return [
        {
            "title": city["etiket__gimt_ja_kalba"]["value"],
            **parse_point(city["koordinat_s"]["value"])
        } for city in content["results"]["bindings"]
    ]


def parse_point(point_str):
    stripped = point_str.lstrip("Point(").rstrip(")")
    split = stripped.split()
    return {
        "longitude": float(split[0]),
        "latitude": float(split[1])
    }
