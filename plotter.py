import matplotlib.pyplot as plt


def plot_solution(solution):
    y = [s["latitude"] for s in solution] + [solution[0]["latitude"]]
    x = [s["longitude"] for s in solution] + [solution[0]["longitude"]]
    plt.plot(x, y)
    for city_info in solution:
        plt.annotate(city_info["title"],
                     xy=(city_info["longitude"], city_info["latitude"]),
                     xytext=(-20, -20), textcoords="offset points",
                     ha="right", va="bottom",
                     bbox={'boxstyle': 'round,pad=0.5', 'fc': 'yellow',
                           'alpha': 0.1},
                     arrowprops={'arrowstyle': '->',
                                 'connectionstyle': 'arc3,rad=0'})
    # plt.gca().invert_yaxis()
    # plt.gca().invert_xaxis()
    plt.show()
