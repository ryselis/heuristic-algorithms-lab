import time

from annealing import Annealer, MultiAnnealer
from file_reader import read_data_from_file
from plotter import plot_solution
from travelling_salesman import TravellingSalesmanProblem


def main():
    data = read_data_from_file("query.json")
    problem = TravellingSalesmanProblem(data)
    for annealer in (Annealer(), MultiAnnealer()):
        for i in range(5):
            result = annealer.anneal(problem)
            plot_solution(result)
            print(problem.get_distance_for_solution(result))



if __name__ == '__main__':
    main()
