import math
import random
from itertools import combinations, product

from annealing import Annealable


class TravellingSalesmanProblem(Annealable):
    def __init__(self, cities):
        self.cities = cities
        self._distance_dict = dict(
            (
                (city1["title"], city2["title"]),
                self._get_distance(city1, city2)
            ) for city1, city2 in product(self.cities, repeat=2)
        )
        sorted_codistances = sorted([
            self._get_distance(city1, city2) for city1, city2 in combinations(self.cities, 2)
        ])
        self._max_distance = sum(sorted_codistances[-len(self.cities):])
        self._min_distance = sum(sorted_codistances[:len(self.cities)])
        self._distance_diff = self._max_distance - self._min_distance

    def objective_function(self, solution) -> float:
        current_distance = self.get_distance_for_solution(solution)
        return (current_distance - self._min_distance) / self._distance_diff

    def get_starting_solution(self):
        return sorted(self.cities, key=lambda x: random.random())

    def get_modified_solution(self, base_solution, allowed_variance):
        amount_of_cities = len(base_solution)
        maximum_move = amount_of_cities * allowed_variance
        randomized_indices = [{
            "coefficient": i + random.random() * maximum_move - maximum_move / 2,
            "city": city
        } for i, city in enumerate(base_solution)]
        corrected_indices = [{
            "coefficient": indice["coefficient"] if indice["coefficient"] > 0
            else indice["coefficient"] + len(self.cities),
            "city": indice["city"]
        } for indice in randomized_indices]
        sorted_cities = sorted(corrected_indices, key=lambda x: x["coefficient"])
        return [entry["city"] for entry in sorted_cities]

    @staticmethod
    def _get_distance(city1, city2):
        earth_radius = 6371
        lat1, lon1 = math.radians(city1["latitude"]), math.radians(city1["longitude"])
        lat2, lon2 = math.radians(city2["latitude"]), math.radians(city2["longitude"])
        return math.acos(math.sin(lat1) * math.sin(lat2) +
                         math.cos(lat1) * math.cos(lat2) * math.cos(lon1 - lon2)) * earth_radius

    def get_distance_for_solution(self, solution):
        try:
            shifted_solution = solution[1:] + [solution[0]]
        except IndexError:
            return 0
        return sum([self._distance_dict[(city1["title"], city2["title"])]
                    for city1, city2
                    in zip(solution, shifted_solution)])

    def get_max_iterations_without_improvement(self) -> int:
        return (len(self.cities) * len(self.cities) - 1) * 100
